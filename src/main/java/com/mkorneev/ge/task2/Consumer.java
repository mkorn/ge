package com.mkorneev.ge.task2;

import java.util.concurrent.TimeUnit;

public class Consumer {

    private final String name;

    public Consumer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void getEvent(Event event) {
        System.out.println(name + " processed: " + event);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

package com.mkorneev.ge.task2;

public class Event implements Comparable<Event> {
    private final String message;
    private final int priority;

    public Event(String message, int priority) {
        this.message = message;
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Event{");
        sb.append("message='").append(message).append('\'');
        sb.append(", priority=").append(priority);
        sb.append('}');
        return sb.toString();
    }


    @Override
    public int compareTo(Event event) {
        return priority - event.getPriority();
    }
}

package com.mkorneev.ge.task2;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class Producer implements Runnable {
    private static final int MAX_PRIORITY = 3;

    private final int batchCount;
    private final int iterationsCount;
    private final int sleepInSec;
    private final Random randomGenerator = new Random();
    private final BlockingQueue<Event> queue;

    public Producer(BlockingQueue<Event> queue,
                    int batchCount,
                    int iterationsCount,
                    int sleepInSec) {
        this.queue = queue;
        this.batchCount = batchCount;
        this.iterationsCount = iterationsCount;
        this.sleepInSec = sleepInSec;
    }

    @Override
    public void run() {
        System.out.println("Start producing ...\n");
        for (int i = 1; i <= iterationsCount; i++) {
            Event event = generateEvent();
            System.out.println("Generated: " + event);
            queue.add(event);
            if (i % batchCount == 0) {
                try {
                    System.out.println("\nProducer is sleep for a second ...\n");
                    TimeUnit.SECONDS.sleep(sleepInSec);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Event generateEvent() {
        return new Event(randomString(), randomPriority());
    }

    private String randomString() {
        return UUID.randomUUID().toString();
    }

    private int randomPriority() {
        return randomGenerator.nextInt(MAX_PRIORITY) + 1;
    }
}

package com.mkorneev.ge.task2.problem2;

import com.mkorneev.ge.task2.Consumer;
import com.mkorneev.ge.task2.Event;

import java.util.concurrent.Callable;

public class SendEventTask implements Callable<String> {

    private final Event event;
    private final Consumer consumer;

    public SendEventTask(Event event, Consumer consumer) {
        this.event = event;
        this.consumer = consumer;
    }

    @Override
    public String call() throws Exception {
        consumer.getEvent(event);
        return String.format("Consumer %s processed event %s", consumer.getName(), event);
    }
}

package com.mkorneev.ge.task2.problem2;

import com.mkorneev.ge.task2.Consumer;
import com.mkorneev.ge.task2.Event;
import com.mkorneev.ge.task2.Producer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

public class Runner {
    private static final int BATCH_COUNT = 5;
    private static final int ITERATIONS_COUNT = 10;
    private static final int SLEEP_IN_SEC = 5;

    public static void main(String[] args) {
        BlockingQueue<Event> queue = new PriorityBlockingQueue<>();

        List<Consumer> consumers = new ArrayList<>();
        consumers.add(new Consumer("B1"));
        consumers.add(new Consumer("B2"));
        consumers.add(new Consumer("B3"));

        ExecutorService executorService = Executors.newCachedThreadPool();
        EventMediator eventMediator = new EventMediator(queue, consumers);
        executorService.execute(eventMediator);
        executorService.execute(new Producer(queue, BATCH_COUNT, ITERATIONS_COUNT, SLEEP_IN_SEC));
        executorService.shutdown();
    }
}

package com.mkorneev.ge.task2.problem2;

import com.mkorneev.ge.task2.Consumer;
import com.mkorneev.ge.task2.Event;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class EventMediator implements Runnable {
    private static final int GET_TIMEOUT_IN_SEC = 30;

    private final BlockingQueue<Event> queue;
    private final List<Consumer> consumers;
    private final ExecutorService executor = Executors.newCachedThreadPool();

    public EventMediator(BlockingQueue<Event> queue, List<Consumer> consumers) {
        this.queue = queue;
        this.consumers = consumers;
    }

    @Override
    public void run() {
        System.out.println("Event mediator start consuming ...\n");
        while (true) {
            try {
                Event event = getEvent();
                if (event == null) {
                    System.out.println("\nEvent mediator stop processing.\n");
                    executor.shutdown();
                    break;
                }
                sendEvent(event);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Event getEvent() throws InterruptedException {
        return queue.poll(GET_TIMEOUT_IN_SEC, TimeUnit.SECONDS);
    }

    private void sendEvent(Event event) {
        List<Future<String>> futures = submitEventTasks(event);
        checkEventTasksComplete(futures);
        System.out.println("Event mediator resend " + event + " to all registered consumers.\n");
    }

    private List<Future<String>> submitEventTasks(Event event) {
        List<Future<String>> results = new ArrayList<>();
        for (Consumer consumer : consumers) {
            Future<String> future = executor.submit(new SendEventTask(event, consumer));
            results.add(future);
        }
        return results;
    }

    private void checkEventTasksComplete(List<Future<String>> futures) {
        for (Future<String> future : futures) {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.mkorneev.ge.task2.problem1;

import com.mkorneev.ge.task2.Consumer;
import com.mkorneev.ge.task2.Event;
import com.mkorneev.ge.task2.Producer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;

public class Runner {
    private static final int BATCH_COUNT = 5;
    private static final int ITERATIONS_COUNT = 15;
    private static final int SLEEP_IN_SEC = 1;

    public static void main(String[] args) {
        BlockingQueue<Event> queue = new PriorityBlockingQueue<>();
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(new SendEventTask(queue, new Consumer("B")));
        executorService.execute(new Producer(queue, BATCH_COUNT, ITERATIONS_COUNT, SLEEP_IN_SEC));
        executorService.shutdown();
    }
}

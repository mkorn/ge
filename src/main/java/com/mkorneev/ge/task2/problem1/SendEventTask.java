package com.mkorneev.ge.task2.problem1;

import com.mkorneev.ge.task2.Consumer;
import com.mkorneev.ge.task2.Event;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class SendEventTask implements Runnable {
    private static final int GET_TIMEOUT_IN_SEC = 30;

    private final Consumer consumer;
    private final BlockingQueue<Event> queue;

    public SendEventTask(BlockingQueue<Event> queue, Consumer consumer) {
        this.queue = queue;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        System.out.println("Send event task start consuming ...");
        while (true) {
            try {
                Event event = getEvent();
                if (event == null) {
                    System.out.println("Send event task stop processing.");
                    break;
                }
                consumer.getEvent(event);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Event getEvent() throws InterruptedException {
        return queue.poll(GET_TIMEOUT_IN_SEC, TimeUnit.SECONDS);
    }
}

package com.mkorneev.ge.task1.fixed;

import com.mkorneev.ge.task1.ValueHolder;

/**
 * Fixed by adding final third-party lockObject to synchronize on it.
 */
public class FixedValueHolder implements ValueHolder {

    private final Object lockObject = new Object();
    private Integer value = 0;

    @Override
    public void incrementValue() {
        synchronized (lockObject) {
            value = value + 1;
        }
    }

    @Override
    public Integer getValue() {
        return value;
    }
}

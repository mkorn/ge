package com.mkorneev.ge.task1;

public interface ValueHolder {
    void incrementValue();
    Integer getValue();
}

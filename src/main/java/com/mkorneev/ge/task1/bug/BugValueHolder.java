package com.mkorneev.ge.task1.bug;

import com.mkorneev.ge.task1.ValueHolder;

/**
 * Synchronization here doesn't work properly, because actually we lock on object currently assigned to the field.
 *
 * Explanation: e.g. thread "A" locks on currently assigned to the value field object (e.g. Integer(0), object "X")
 * and starts to increment it, other threads are waiting when thread "A" will free monitor of object "X".
 * When thread "A" has incremented field and assigned new object to it (object "Y"), all other threads now allowed
 * to get access to synchronized block (because they locked by old object "X", not new object "Y").
 *
 * So data races occurs.
 */
public class BugValueHolder implements ValueHolder {

    private Integer value = 0;

    @Override
    public void incrementValue() {
        synchronized (value) {
            value = value + 1;
        }
    }

    @Override
    public Integer getValue() {
        return value;
    }
}

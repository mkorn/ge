package com.mkorneev.ge.task1;

public class IncrementTask implements Runnable {

    private ValueHolder valueHolder;

    public IncrementTask(ValueHolder valueHolder) {
        this.valueHolder = valueHolder;
    }

    @Override
    public void run() {
        valueHolder.incrementValue();
    }
}

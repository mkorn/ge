package com.mkorneev.ge.task1;

import com.mkorneev.ge.task1.bug.BugValueHolder;
import com.mkorneev.ge.task1.fixed.FixedValueHolder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Runner {
    private static final int THREAD_COUNT = 1000;

    public static void main(String[] args) {
        valueHolderTest(new BugValueHolder());
        valueHolderTest(new FixedValueHolder());
    }

    private static void valueHolderTest(ValueHolder valueHolder) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < THREAD_COUNT; i++) {
            executorService.execute(new IncrementTask(valueHolder));
        }
        executorService.shutdown();
        System.out.println("Result: " + valueHolder.getValue());
    }
}
